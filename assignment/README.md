# Course Assignment
## What? 
Assignment is to develop Spring Boot Application that exposes RESTful API for submiting registration for **Summer cooking school**.
 
This assignment will validate that you have mastered concepts from online course and also prepare you for final test that you will receive for Hackathon.

Each attendee is assigned a mentor that will evaluate your progress leading to Hackathon.
___
Each week before Hackathon has weekly assignment, and each of them has 2 parts:

- Primary Quest
- Side Quests

### Primary Quest
This must be completed for weekly assignment to be considered complete. If not completed in timely fashion, that might be grounds for disqualification.

### Side Quests
These are optional tasks that could be completed.
While it is possible to complete primary quest without completing all or any of the side quests (*N.B. this is intentional*), it is advised to complete as many as you can.

## How?
1. Create your own repository from project template.
2. Create separate branch for weekly assignment.
3. Implement weekly assignment.
5. Create merge request.
6. Iterate over solution until your assigned mentor determines weekly assignment is acceptable.

## When?
Weekly assignemnt should be completed at the end of that week, any submission outside of this timebox will be evaluated on individual basis.