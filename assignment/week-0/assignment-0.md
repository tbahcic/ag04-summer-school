# Week Zero Assignment

## Setup development environment
### Linux

1. Install Java JDK 12 : http://jdk.java.net/12/
2. Install Maven 3.6.0 or higher: https://maven.apache.org/install.html
3. Install Gradle 5.2.1 or higher: https://gradle.org/install/
4. Install Git: https://www.atlassian.com/git/tutorials/install-git
5. Install IntelliJ IDEA CE: https://www.jetbrains.com/idea/download


### Windows

1. Install Java JDK 12: http://jdk.java.net/12/
2. Install Maven 3.6.0 or higher: https://maven.apache.org/install.html
3. Install Gradle 5.2.1 or higher: https://gradle.org/install/
4. Install Git: https://www.atlassian.com/git/tutorials/install-git
5. Install IntelliJ IDEA CE: https://www.jetbrains.com/idea/download

### macOS

1. Install Homebrew: https://brew.sh/
2. Install Java JDK 12: https://tecadmin.net/install-java-macos/
3. Install Maven 3.6.0 or higher: http://brewformulas.org/Maven  
4. Install Gradle 5.2.1 or higher: https://gradle.org/install/
5. Install Git: https://www.atlassian.com/git/tutorials/install-git
6. Install IntelliJ IDEA CE: https://www.jetbrains.com/idea/download


### Verify installation
Run each command from command line:

```
git --version
java -version
javac -version
gradle -v
mvn -v
```

### Setup your account and repository
1. Setup GitLab account
2. Checkout repository with assignment
3. Create your own repository from Project Template
4. Create your own branch
5. Add your mentors as member to your repository with role Maintainer

Video instructions:

------------

#### Covered lectures  
1. Introduction to Spring Framework 5: Beginner to Guru  
    - Spring Framework 5 - Course Introduction
    - Instructor Introduction - John Thompson, Spring Framework Guru
    - Setting up your Development Environment for Spring
    - Is Your IDE Free Like a Puppy?
    - Free 90 Day IntelliJ IDEA Trial!
    - What's New in Spring Framework 5? 
    - Getting Help with the Spring Framework
    - Course Slack Room - Chat Live with Me and Other Gurus!
2. Building a Spring Boot Web App  
    - Introduction
    - Spring Initializr
    - Open Project in IntelliJ