package com.ag04smarts.scc.model.socialmedia;

public class Facebook {

    private String url;

    public Facebook() {}

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
