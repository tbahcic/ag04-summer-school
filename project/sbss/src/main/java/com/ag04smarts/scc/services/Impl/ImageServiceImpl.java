package com.ag04smarts.scc.services.Impl;

import com.ag04smarts.scc.model.Candidate;
import com.ag04smarts.scc.repositories.CandidateRepository;
import com.ag04smarts.scc.services.ImageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Slf4j
@Service
public class ImageServiceImpl implements ImageService {

    private final CandidateRepository candidateRepository;

    @Autowired
    public ImageServiceImpl( CandidateRepository candidateRepository) {
        this.candidateRepository = candidateRepository;
    }

    @Override
    public void saveImageFile(Long candidateId, MultipartFile file) {

        try {

            Candidate candidate = candidateRepository.findById(candidateId).get();

            Byte[] byteObjects = new Byte[file.getBytes().length];

            int i = 0;

            for(byte b : file.getBytes()){
                byteObjects[i++] = b;
            }

            candidate.setImage(byteObjects);
            Candidate candidate2 = candidateRepository.save(candidate);

        } catch (IOException e) {

            log.error("Error occurred", e);
            e.printStackTrace();
        }
    }
}
