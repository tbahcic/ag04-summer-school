package com.ag04smarts.scc.services.Impl;

import com.ag04smarts.scc.converters.RegistrationFormToCandidate;
import com.ag04smarts.scc.converters.RegistrationFormToRegistration;
import com.ag04smarts.scc.model.Candidate;
import com.ag04smarts.scc.model.Course;
import com.ag04smarts.scc.model.Registration;
import com.ag04smarts.scc.model.RegistrationForm;
import com.ag04smarts.scc.repositories.CandidateRepository;
import com.ag04smarts.scc.repositories.CourseRepository;
import com.ag04smarts.scc.repositories.RegistrationRepository;
import com.ag04smarts.scc.services.RegistrationFormService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
public class RegistrationFormServiceImpl implements RegistrationFormService {

    private RegistrationFormToCandidate registrationFormToCandidate;
    private RegistrationFormToRegistration registrationFormToRegistration;
    private CandidateRepository candidateRepository;
    private RegistrationRepository registrationRepository;

    public RegistrationFormServiceImpl(RegistrationFormToCandidate registrationFormToCandidate, RegistrationFormToRegistration registrationFormToRegistration,
            CandidateRepository candidateRepository, RegistrationRepository registrationRepository) {
        this.registrationFormToCandidate = registrationFormToCandidate;
        this.registrationFormToRegistration = registrationFormToRegistration;
        this.candidateRepository = candidateRepository;
        this.registrationRepository = registrationRepository;
    }

    @Override
    @Transactional
    public Registration saveRegistrationForm(RegistrationForm command){

        Registration detachedRegistration = registrationFormToRegistration.convert(command);
        Registration registration = registrationRepository.save(detachedRegistration);

        Candidate detachedCandidate = registrationFormToCandidate.convert(command);
        detachedCandidate.setRegistration(registration);
        Candidate candidate = candidateRepository.save(detachedCandidate);

        log.debug("Saved registration:" + registration.getId());
        log.debug("Saved candidate:" + candidate.getId());

        return detachedRegistration;

    }

}
