package com.ag04smarts.scc.services.Impl;

import com.ag04smarts.scc.services.StatusInfo;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("en")
public class StatusInfoEnglish implements StatusInfo {

    @Override
    public String saving() {
        return "Data is saved.";
    }

    @Override
    public String fetching(){
        return "Data is fetched.";
    }
}
