package com.ag04smarts.scc.bootstrap;

import com.ag04smarts.scc.model.*;
import com.ag04smarts.scc.repositories.CandidateRepository;
import com.ag04smarts.scc.repositories.CourseRepository;
import com.ag04smarts.scc.repositories.LecturerRepository;
import com.ag04smarts.scc.repositories.RegistrationRepository;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.mapping.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashSet;

@Slf4j
@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private CandidateRepository candidateRepository;
    private LecturerRepository lecturerRepository;
    private CourseRepository courseRepository;
    private RegistrationRepository registrationRepository;


    @Autowired
    public DevBootstrap(CandidateRepository candidateRepository, LecturerRepository lecturerRepository, CourseRepository courseRepository,
            RegistrationRepository registrationRepository) {
        this.candidateRepository = candidateRepository;
        this.lecturerRepository = lecturerRepository;
        this.courseRepository = courseRepository;
        this.registrationRepository = registrationRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent){
        initData();
    }

    private void initData() {

        Lecturer gordon = new Lecturer("Gordon", "Ramsey");
        Lecturer jamie = new Lecturer("Jamie", "Oliver");
        Lecturer julia = new Lecturer("Julia", "Child");

        lecturerRepository.save(gordon);
        lecturerRepository.save(jamie);
        lecturerRepository.save(julia);

        java.util.Set<Lecturer> learnToCookLecturers = new HashSet<Lecturer>();
        learnToCookLecturers.add(gordon);
        learnToCookLecturers.add(jamie);

        Course learnToCook = new Course("Learn to cook", CourseType.BASIC, 8, learnToCookLecturers);

        java.util.Set<Lecturer> mexicanCookingLecturers = new HashSet<Lecturer>();
        learnToCookLecturers.add(gordon);
        learnToCookLecturers.add(julia);

        Course mexicanCooking = new Course("Mexican cooking", CourseType.BASIC, 3, mexicanCookingLecturers);

        java.util.Set<Lecturer> cookingWithStyleLecturers = new HashSet<Lecturer>();
        learnToCookLecturers.add(jamie);
        learnToCookLecturers.add(julia);

        Course cookingWithStyle = new Course("Cooking with style", CourseType.INTERMEDIATE, 5, cookingWithStyleLecturers);

        java.util.Set<Lecturer> jamieOliverCookingCourseLecturers = new HashSet<Lecturer>();
        learnToCookLecturers.add(gordon);
        learnToCookLecturers.add(jamie);

        Course jamieOliverCookingCourse = new Course("Jemie Oliver cooking course", CourseType.ADVANCED, 3, jamieOliverCookingCourseLecturers);

        courseRepository.save(learnToCook);
        courseRepository.save(mexicanCooking);
        courseRepository.save(cookingWithStyle);
        courseRepository.save(jamieOliverCookingCourse);


        java.util.Set<Course> registrationBeforeCourses = new HashSet<Course>();
        registrationBeforeCourses.add(cookingWithStyle);
        registrationBeforeCourses.add(jamieOliverCookingCourse);

        java.util.Set<Course> registrationAfterCourses = new HashSet<Course>();
        registrationAfterCourses.add(mexicanCooking);
        registrationAfterCourses.add(learnToCook);

        Registration registrationAfter = new Registration(new Date(2019, 6, 31), registrationBeforeCourses);
        Registration registrationAfter2 = new Registration(new Date(2019, 6, 31), registrationBeforeCourses);
        Registration registrationBefore = new Registration(new Date(2019, 6, 11), registrationAfterCourses);
        Registration registrationBefore2 = new Registration(new Date(2019, 6, 11), registrationAfterCourses);
        Registration registrationBefore3 = new Registration(new Date(2019, 6, 11), registrationAfterCourses);
        registrationRepository.save(registrationAfter);
        registrationRepository.save(registrationAfter2);
        registrationRepository.save(registrationBefore);
        registrationRepository.save(registrationBefore2);
        registrationRepository.save(registrationBefore3);


        Candidate ivan = new Candidate("Ivan", "Horvat", 20, Gender.MALE, registrationAfter);
        Candidate john = new Candidate("John", "Doe", 26, Gender.MALE, registrationAfter2);
        Candidate jane = new Candidate("Jane", "Doe", 32, Gender.FEMALE, registrationBefore);
        Candidate ivana = new Candidate("Ivana", "Horvat", 19, Gender.FEMALE, registrationBefore2);
        Candidate iva = new Candidate("Iva", "Horvatincic", 46, Gender.FEMALE, registrationBefore3);

//        Candidate ivan = new Candidate("Ivan", "Horvat", 20, Gender.MALE);
//        Candidate john = new Candidate("John", "Doe", 26, Gender.MALE);
//        Candidate jane = new Candidate("Jane", "Doe", 32, Gender.FEMALE);
//        Candidate ivana = new Candidate("Ivana", "Horvat", 19, Gender.FEMALE);
//        Candidate iva = new Candidate("Iva", "Horvatincic", 46, Gender.FEMALE);

        candidateRepository.save(ivan);
        candidateRepository.save(john);
        candidateRepository.save(jane);
        candidateRepository.save(ivana);
        candidateRepository.save(iva);

//        Registration registrationAfter = new Registration(new Date(2019, 6, 31), registrationBeforeCourses, ivan);
//        Registration registrationAfter2 = new Registration(new Date(2019, 6, 31), registrationBeforeCourses, john);
//        Registration registrationBefore = new Registration(new Date(2019, 6, 11), registrationAfterCourses, jane);
//        Registration registrationBefore2 = new Registration(new Date(2019, 6, 11), registrationAfterCourses, ivana);
//        Registration registrationBefore3 = new Registration(new Date(2019, 6, 11), registrationAfterCourses, iva);
//
//        registrationRepository.save(registrationAfter);
//        registrationRepository.save(registrationAfter2);
//        registrationRepository.save(registrationBefore);
//        registrationRepository.save(registrationBefore2);
//        registrationRepository.save(registrationBefore3);

        log.debug("Data is initialized.");

    }
}
