package com.ag04smarts.scc;

import com.ag04smarts.scc.controllers.CandidateController;
import com.ag04smarts.scc.model.socialmedia.Facebook;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ImportResource;

import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootApplication
@ImportResource("classpath:lunch-config.xml")
public class SCCApplication {

	public static void main(String[] args) {

		ApplicationContext ctx =  SpringApplication.run(SCCApplication.class, args);
		CandidateController candidateController = (CandidateController) ctx.getBean("candidateController");

		Facebook facebook = (Facebook) ctx.getBean(Facebook.class);
		System.out.println(facebook.getUrl());

		System.out.println(new Date(2019, 6, 31));

	}
}
