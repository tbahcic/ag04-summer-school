package com.ag04smarts.scc.controllers;

import com.ag04smarts.scc.model.Candidate;
import com.ag04smarts.scc.model.Registration;
import com.ag04smarts.scc.model.RegistrationForm;
import com.ag04smarts.scc.repositories.CandidateRepository;
import com.ag04smarts.scc.repositories.RegistrationRepository;
import com.ag04smarts.scc.services.RegistrationFormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("/api")
public class RegistrationController {

    private RegistrationFormService registrationFormService;

    @Autowired
    public RegistrationController(RegistrationFormService registrationFormService) {
        this.registrationFormService = registrationFormService;
    }

    @PostMapping("/registration")
    public Registration register(@Valid @RequestBody RegistrationForm registrationForm){

        return registrationFormService.saveRegistrationForm(registrationForm);
    }

}
