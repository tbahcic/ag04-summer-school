package com.ag04smarts.scc.model.socialmedia;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:scc.properties")
public class SocialNetworks {

    @Value("${school.url.linkedin}")
    private String linkedin;
    @Value("${school.url.twitter}")
    private String twitter;

}
