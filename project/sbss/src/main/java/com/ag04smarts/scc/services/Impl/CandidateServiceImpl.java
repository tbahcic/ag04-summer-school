package com.ag04smarts.scc.services.Impl;

import com.ag04smarts.scc.model.Candidate;
import com.ag04smarts.scc.repositories.CandidateRepository;
import com.ag04smarts.scc.services.CandidateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Slf4j
@Service
public class CandidateServiceImpl implements CandidateService {

    private final CandidateRepository candidateRepository;

    public CandidateServiceImpl(CandidateRepository candidateRepository) {
        this.candidateRepository = candidateRepository;
    }

    @Override
    public Set<Candidate> getCandidates() {
        log.debug("I'm in the service");

        Set<Candidate> candidateSet = new HashSet<>();
        candidateRepository.findAll().iterator().forEachRemaining(candidateSet::add);
        return candidateSet;
    }

}
