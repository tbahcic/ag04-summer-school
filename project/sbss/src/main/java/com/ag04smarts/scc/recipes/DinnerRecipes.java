package com.ag04smarts.scc.recipes;

import java.util.ArrayList;
import java.util.List;

public class DinnerRecipes {
    private List<String> recipes;

    public List<String> getRecipes() {
        return recipes;
    }

    public void setRecipes(ArrayList<String> recipes) {
        this.recipes = recipes;
    }

    public DinnerRecipes(){
        recipes = new ArrayList<String>();
        recipes.add("Eggs and ham");
        recipes.add("Eggs and sausages");
        recipes.add("Eggs and cheese");
    }

}
