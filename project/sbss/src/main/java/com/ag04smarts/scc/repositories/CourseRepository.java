package com.ag04smarts.scc.repositories;

import com.ag04smarts.scc.model.Course;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CourseRepository extends CrudRepository<Course, Long> {

    Course findCourseById(Long id);
}
