package com.ag04smarts.scc.services;

import com.ag04smarts.scc.model.Candidate;

import java.util.Set;

public interface CandidateService {

    Set<Candidate> getCandidates();
}
