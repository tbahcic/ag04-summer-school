package com.ag04smarts.scc.controllers;

import com.ag04smarts.scc.recipes.DinnerRecipes;
import com.ag04smarts.scc.services.Cookbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/cookbook")
public class CookbookDinnerController {

    private Cookbook cookbook;

    @Autowired
    public CookbookDinnerController(@Qualifier("dinnerCookbook") Cookbook cookbook) {
        this.cookbook = cookbook;
    }

    @GetMapping("/dinner")
    public List<String> getRecipes(){
        cookbook.containsMeal("ham");
        cookbook.containsMealArgumentAutowireExample("ham");
        return cookbook.getRecipes();
    }

}
