package com.ag04smarts.scc.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;

@Data
@Entity
public class Candidate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private Integer age;

    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @ManyToOne
    private Mentor mentor;

    @OneToOne
    @JsonBackReference
    private Registration registration;

    @Lob
    private Byte[] image;

    public Candidate(){}

    public Candidate(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Candidate(String firstName, String lastName, String email, Registration registration) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.registration = registration;
    }

    public Candidate(String firstName, String lastName, Integer age, Gender gender, Registration registration) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.gender = gender;
        this.registration = registration;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof Candidate;
    }

}
