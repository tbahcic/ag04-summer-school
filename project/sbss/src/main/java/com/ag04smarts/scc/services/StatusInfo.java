package com.ag04smarts.scc.services;

public interface StatusInfo {

    String saving();
    String fetching();

}
