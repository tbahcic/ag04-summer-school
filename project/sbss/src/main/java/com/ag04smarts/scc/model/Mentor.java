package com.ag04smarts.scc.model;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
public class Mentor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;
    private String lastName;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mentor")
    private Set<Candidate> candidates;

    public Mentor(){}

}
