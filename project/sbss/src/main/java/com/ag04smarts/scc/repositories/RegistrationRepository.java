package com.ag04smarts.scc.repositories;

import com.ag04smarts.scc.model.Registration;
import org.springframework.data.repository.CrudRepository;

public interface RegistrationRepository extends CrudRepository<Registration, Long> {
}
