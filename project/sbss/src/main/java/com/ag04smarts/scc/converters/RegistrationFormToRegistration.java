package com.ag04smarts.scc.converters;

import com.ag04smarts.scc.model.*;
import com.ag04smarts.scc.repositories.CandidateRepository;
import com.ag04smarts.scc.repositories.CourseRepository;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashSet;
import java.util.List;

@Component
public class RegistrationFormToRegistration implements Converter<RegistrationForm, Registration> {

    private CourseRepository courseRepository;
    private CandidateRepository candidateRepository;

    public RegistrationFormToRegistration(CourseRepository courseRepository, CandidateRepository candidateRepository) {
        this.courseRepository = courseRepository;
        this.candidateRepository = candidateRepository;
    }

    @Synchronized
    @Nullable
    @Override
    public Registration convert(RegistrationForm source) {
        if (source == null) {
            return null;
        }

        final Registration registration = new Registration();
        registration.setRegistrationDate(source.getRegistrationDate());

        Course course = courseRepository.findCourseById(source.getCourseId());
        List<Candidate> candidates = candidateRepository.findCandidatesAttendingCourse();

            if (candidates.size() < course.getNumberOfStudents()) {
                java.util.Set<Course> courses = new HashSet<>();
                courses.add(course);

                registration.setCourses(courses);
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Course is full!");
            }


        return registration;
    }

}
