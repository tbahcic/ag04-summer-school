package com.ag04smarts.scc.services.Impl;

import com.ag04smarts.scc.services.QualifierExample;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
@Primary
public class QualifierExamplePrimary implements QualifierExample {

    @Override
    public String qualifierType() {
        return "I am primary qualifier!";
    }
}
