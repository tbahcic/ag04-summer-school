package com.ag04smarts.scc.services.Impl;

import com.ag04smarts.scc.services.StatusInfo;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("hr")
public class StatusInfoCroatian implements StatusInfo {

    @Override
    public String saving() {
        return "Podaci su spremljeni.";
    }

    @Override
    public String fetching(){
        return "Podaci su dohvaćeni.";
    }

}
