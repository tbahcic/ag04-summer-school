package com.ag04smarts.scc.controllers;

import com.ag04smarts.scc.services.ImageService;
import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController
@RequestMapping("/api")
public class ImageController {

    private final ImageService imageService;

    @Autowired
    public ImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    @PostMapping("/candidates/{candidateId}/image")
    public void handleImagePost(@PathVariable Long candidateId, @RequestParam("imagefile") MultipartFile file){

        imageService.saveImageFile(candidateId, file);

    }

}
