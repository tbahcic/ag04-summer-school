package com.ag04smarts.scc.config;

import com.ag04smarts.scc.model.socialmedia.Facebook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
public class PropertyConfig {

    @Value("${school.url.facebook}")
    private String facebookUrl;

    @Bean
    public Facebook getFacebookInfo(){
        Facebook facebook = new Facebook();
        facebook.setUrl(facebookUrl);
        return facebook;
    }

}
