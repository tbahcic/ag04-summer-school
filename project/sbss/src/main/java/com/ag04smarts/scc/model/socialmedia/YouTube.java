package com.ag04smarts.scc.model.socialmedia;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class YouTube {

    private String baseUrl;
    private String username;
    private Boolean isVlogger;

    public YouTube(){}
    public YouTube(@Value("${default.youtube.baseUrl}") final String baseUrl,
                     @Value("${default.youtube.isVlogger}") final Boolean isVlogger) {
        this.baseUrl = baseUrl;
        this.isVlogger = isVlogger;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getVlogger() {
        return isVlogger;
    }

    public void setVlogger(Boolean vlogger) {
        isVlogger = vlogger;
    }
}
