package com.ag04smarts.scc.controllers;

import com.ag04smarts.scc.services.Cookbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/cookbook")
public class CookbookLunchController {

    private Cookbook cookbook;

    @Autowired
    public CookbookLunchController(@Qualifier("lunchCookbook") Cookbook cookbook) {
        this.cookbook = cookbook;
    }

    @GetMapping("/lunch")
    public List<String> getRecipes(){
        return cookbook.getRecipes();
    }

}
