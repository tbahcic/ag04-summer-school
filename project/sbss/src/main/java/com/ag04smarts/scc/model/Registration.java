package com.ag04smarts.scc.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Data
@Entity
public class Registration {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Date registrationDate;

    @ManyToMany
    @JoinTable(name = "registration_course",
            joinColumns = @JoinColumn(name = "registration_id"),
            inverseJoinColumns = @JoinColumn(name = "course_id"))
    @JsonManagedReference
    private Set<Course> courses;

    public Registration(){}

    public Registration(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Registration(Date registrationDate, Set<Course> courses) {
        this.registrationDate = registrationDate;
        this.courses = courses;
    }



}
