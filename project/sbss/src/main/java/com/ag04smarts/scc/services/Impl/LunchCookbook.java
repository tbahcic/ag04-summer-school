package com.ag04smarts.scc.services.Impl;

import com.ag04smarts.scc.recipes.LunchRecipes;
import com.ag04smarts.scc.services.Cookbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LunchCookbook implements Cookbook {

    private final LunchRecipes lunchRecipes;

    public LunchCookbook(LunchRecipes lunchRecipes){
        this.lunchRecipes = lunchRecipes;
    }

    @Override
    public List<String> getRecipes() {
        return lunchRecipes.getRecipes();
    }

    // Prilikom inicijalizacije konteksta pozvati će se ova metoda s parametrom iz application.properies datoteke
    // sa vrijednošću varijable default.meal
    @Value("${default.meal}")
    public boolean containsMeal(String mealName){
        List<String> recipes = this.getRecipes();
        System.out.println(mealName);
        System.out.println(recipes.contains(mealName));
        return recipes.contains(mealName);
    }

    public boolean containsMealArgumentAutowireExample(@Value("${default.meal}") String mealName){
        List<String> recipes = this.getRecipes();
        System.out.println(mealName);
        System.out.println(recipes.contains(mealName));
        return recipes.contains(mealName);
    }
}
