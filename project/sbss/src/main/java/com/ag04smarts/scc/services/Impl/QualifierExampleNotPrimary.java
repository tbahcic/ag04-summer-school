package com.ag04smarts.scc.services.Impl;

import com.ag04smarts.scc.services.QualifierExample;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class QualifierExampleNotPrimary implements QualifierExample {

    @Override
    public String qualifierType() {
        return "I am not primary qualifier!";
    }
}
