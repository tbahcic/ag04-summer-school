package com.ag04smarts.scc.model;

public enum CourseType {

    BASIC, ADVANCED, INTERMEDIATE
}
