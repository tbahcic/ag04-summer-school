package com.ag04smarts.scc.controllers;

import com.ag04smarts.scc.model.socialmedia.YouTube;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class YouTubeController {

    private YouTube youTube;

    @Autowired
    public YouTubeController(YouTube youTube){
        this.youTube = youTube;
    }

    @PostMapping("/api/youtube")
    public void newCandidate(String username){
        youTube.setUsername(username);
        System.out.println(youTube.getBaseUrl());
    }

}
