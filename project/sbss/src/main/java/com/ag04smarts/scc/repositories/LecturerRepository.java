package com.ag04smarts.scc.repositories;

import com.ag04smarts.scc.model.Lecturer;
import org.springframework.data.repository.CrudRepository;

public interface LecturerRepository extends CrudRepository<Lecturer, Long> {
}
