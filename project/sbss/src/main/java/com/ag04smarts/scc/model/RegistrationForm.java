package com.ag04smarts.scc.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.validation.constraints.*;
import java.util.Calendar;
import java.util.Date;

@Getter
@Setter
public class RegistrationForm {

    @NotBlank
    @Size(min = 3, max = 255)
    private String firstName;

    @NotBlank
    @Size(min = 2, max = 35)
    private String lastName;

    @NotBlank
    @Email
    private String email;

    @NotNull
    private Long courseId;

    private Date registrationDate;

//    {
//        "firstName": "Ivan",
//            "lastName": "Hrvatska",
//            "email": "a@a.com",
//            "courseId": 1
//    }

    public RegistrationForm() {
        this.registrationDate = new Date(System.currentTimeMillis());
    }
}
