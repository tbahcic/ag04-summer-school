package com.ag04smarts.scc.services.Impl;

import com.ag04smarts.scc.recipes.DinnerRecipes;
import com.ag04smarts.scc.services.Cookbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Primary
public class DinnerCookbook implements Cookbook {

    private final DinnerRecipes dinnerRecipes;

    public DinnerCookbook(DinnerRecipes dinnerRecipes){
        this.dinnerRecipes = dinnerRecipes;
    }

    @Override
    public List<String> getRecipes() {
        return dinnerRecipes.getRecipes();
    }

    // Prilikom inicijalizacije konteksta pozvati će se ova metoda s parametrom iz application.properies datoteke
    // sa vrijednošću varijable default.meal
    @Value("${default.meal}")
    public boolean containsMeal(String mealName){
        List<String> recipes = this.getRecipes();
        System.out.println(mealName);
        System.out.println(recipes.contains(mealName));
        return recipes.contains(mealName);
    }

    public boolean containsMealArgumentAutowireExample(@Value("${default.meal}") String mealName){
        List<String> recipes = this.getRecipes();
        System.out.println(mealName);
        System.out.println(recipes.contains(mealName));
        return recipes.contains(mealName);
    }
}
