package com.ag04smarts.scc.model;

public enum Gender {

    MALE, FEMALE
}
