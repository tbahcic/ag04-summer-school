package com.ag04smarts.scc.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Data
@Entity
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Enumerated(value = EnumType.STRING)
    private CourseType type;

    private Integer numberOfStudents;

    @ManyToMany
    @JoinTable(name = "course_lecturer",
            joinColumns = @JoinColumn(name = "course_id"),
            inverseJoinColumns = @JoinColumn(name = "lecturer_id"))
    @JsonBackReference
    private Set<Lecturer> lecturers;

    @ManyToMany(mappedBy = "courses")
    @JsonBackReference
    private Set<Registration> registrations;

    public Course() {
    }

    public Course(String name, CourseType type, Integer numberOfStudents, Set<Lecturer> lecturers) {
        this.name = name;
        this.type = type;
        this.numberOfStudents = numberOfStudents;
        this.lecturers = lecturers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return Objects.equals(id, course.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
