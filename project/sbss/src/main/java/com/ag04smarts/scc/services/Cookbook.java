package com.ag04smarts.scc.services;

import org.springframework.beans.factory.annotation.Value;

import java.util.ArrayList;
import java.util.List;

public interface Cookbook {

    List<String> getRecipes();
    boolean containsMeal(String mealName);
    boolean containsMealArgumentAutowireExample(String mealName);
}
