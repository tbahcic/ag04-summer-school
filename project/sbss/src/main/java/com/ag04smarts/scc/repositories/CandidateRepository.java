package com.ag04smarts.scc.repositories;

import com.ag04smarts.scc.model.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface CandidateRepository extends CrudRepository<Candidate, Long> {

    List<Candidate> findAllByAgeGreaterThanAndRegistration_RegistrationDateAfter(Integer age, Date date);


    @Query(value = "SELECT DISTINCT candidate.* FROM candidate " +
            "JOIN registration_course ON registration_course.registration_id = candidate.registration_id " +
            "JOIN course ON course.id = registration_course.course_id " +
            "WHERE course.type = 'BASIC' AND candidate.gender = 'FEMALE' ",
            nativeQuery=true
    )
    List<Candidate> genderAndCourseType();

    Candidate findCandidateById(Long id);

    @Query(value = "SELECT DISTINCT candidate.* FROM candidate " +
            "JOIN registration_course ON registration_course.registration_id = candidate.registration_id " +
            "JOIN course ON course.id = registration_course.course_id " +
            "WHERE course.id = 1",
            nativeQuery=true
    )
    List<Candidate> findCandidatesAttendingCourse();
//    List<Candidate> findCandidatesAttendingCourse(@Param("courseId") Long courseId);

}

