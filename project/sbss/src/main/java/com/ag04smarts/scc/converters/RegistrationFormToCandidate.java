package com.ag04smarts.scc.converters;

import com.ag04smarts.scc.model.Candidate;
import com.ag04smarts.scc.model.RegistrationForm;
import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RegistrationFormToCandidate implements Converter<RegistrationForm, Candidate> {

    @Synchronized
    @Nullable
    @Override
    public Candidate convert(RegistrationForm source) {
        if (source == null) {
            return null;
        }

        log.debug("Converted working!!!");


        final Candidate candidate = new Candidate();

        candidate.setFirstName(source.getFirstName());
        candidate.setLastName(source.getLastName());
        candidate.setEmail(source.getEmail());

        return candidate;
    }

}
