package com.ag04smarts.scc.config;

import com.ag04smarts.scc.recipes.DinnerRecipes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DinnerRecipesConfig {

    @Bean
    public DinnerRecipes dinnerRecipes() {
        return new DinnerRecipes();
    }

}
