package com.ag04smarts.scc.controllers;

import com.ag04smarts.scc.exceptions.NotFoundException;
import com.ag04smarts.scc.model.CourseType;
import com.ag04smarts.scc.model.Gender;
import com.ag04smarts.scc.model.Registration;
import com.ag04smarts.scc.repositories.CandidateRepository;

import com.ag04smarts.scc.services.CandidateService;
import com.ag04smarts.scc.services.QualifierExample;
import com.ag04smarts.scc.services.StatusInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import com.ag04smarts.scc.model.Candidate;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api")
public class CandidateController implements ApplicationContextAware {

    private CandidateRepository candidateRepository;
    private StatusInfo statusInfo;
    private QualifierExample qualifierExample;

    @Autowired
    public CandidateController(CandidateRepository candidateRepository, StatusInfo statusInfo, @Qualifier("qualifierExampleNotPrimary")  QualifierExample qualifierExample) {
        this.candidateRepository = candidateRepository;
        this.statusInfo = statusInfo;
        this.qualifierExample = qualifierExample;
    }

    @PostConstruct
    private void postConstruct() {
        System.out.println(qualifierExample.qualifierType());
    }

    @GetMapping("/candidates")
    public List<Candidate> getCandidate(){

        List<Candidate> candidates = (List<Candidate>) candidateRepository.findAll();
        System.out.println(statusInfo.fetching());
        return candidates;
    }

    @GetMapping("/candidates/{id}")
    public Candidate getCandidateById(@PathVariable(value = "id") Long id){

        Candidate candidate = candidateRepository.findCandidateById(id);
        if(candidate == null){
            throw new NotFoundException("There is no candidate with given id: " + id);
        }
        return candidate;
    }

    @PostMapping("/candidates")
    public Candidate newCandidate(@RequestBody Candidate newCandidate){

        Candidate candidate = candidateRepository.save(newCandidate);
        System.out.println(statusInfo.saving());
        return candidate;
    }

    @PutMapping("/candidates/{id}")
    public Candidate putCandidate(@PathVariable(value = "id") Long id, @RequestBody Candidate candidateData){

        Candidate candidate =  candidateRepository.findCandidateById(id);

        candidate.setFirstName(candidateData.getFirstName());
        candidate.setLastName(candidateData.getLastName());
        candidate.setPhoneNumber(candidateData.getPhoneNumber());
        candidate.setEmail(candidateData.getEmail());
        candidate.setAge(candidateData.getAge());
        candidate.setGender(candidateData.getGender());
        candidate.setMentor(candidateData.getMentor());

        return candidateRepository.save(candidate);

    }

    @DeleteMapping("/candidates/{id}")
    public ResponseEntity deleteCandidate(@PathVariable(value = "id") Long id){

        Candidate candidate =  candidateRepository.findCandidateById(id);
        candidateRepository.delete(candidate);

        return ResponseEntity.ok().build();
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("Application context has been set");
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity handleNotFound(RuntimeException ex){

        log.error("Handling not found exception");
        log.error(toString(ex));

        return ResponseEntity.notFound().build();
    }

    public String toString(RuntimeException ex)
    {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("error message", ex.getLocalizedMessage())
                .append("Http status response", 404).
                toString();
    }
}
