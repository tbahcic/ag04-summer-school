package com.ag04smarts.scc.recipes;

import java.util.ArrayList;
import java.util.List;

public class LunchRecipes {
    private List<String> recipes;

    public List<String> getRecipes() {
        return recipes;
    }

    public void setRecipes(ArrayList<String> recipes) {
        this.recipes = recipes;
    }

    public LunchRecipes(){
        recipes = new ArrayList<String>();
        recipes.add("Pork and potatos");
        recipes.add("Tomatos and fries");
        recipes.add("Cheese and ham");
    }
}
