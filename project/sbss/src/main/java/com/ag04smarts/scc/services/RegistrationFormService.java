package com.ag04smarts.scc.services;

import com.ag04smarts.scc.model.Registration;
import com.ag04smarts.scc.model.RegistrationForm;

public interface RegistrationFormService {

    Registration saveRegistrationForm(RegistrationForm command);
}
